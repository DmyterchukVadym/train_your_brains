using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game_Controller : MonoBehaviour
{
    [SerializeField] private Text text_Task_A;
    [SerializeField] private Text text_Task_Char;
    [SerializeField] private Text text_Task_B;
    [SerializeField] private Button[] buttons;
    private Text[] buttons_Text;
    private char[] signs = new char[] { '+', '-', '*'};

    private int numb_A;
    private char sign;
    private int numb_B;
    private float result_Task;

    private float text_Next_Button;
    private int numb_Winner_Button;
    private int count_Signs_Numbers;

    public float Result_Task { get {return result_Task;} }

    private void Start()
    {
        buttons_Text = new Text[buttons.Length];
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons_Text[i] = buttons[i].GetComponentInChildren<Text>();
        }
        count_Signs_Numbers = 1;
        Set_Next_Question();
    }

    private void Set_Question()
    {
        switch (count_Signs_Numbers)
        {
            case 1:
                text_Task_A.text = $"{numb_A = Random.Range(0, 10)}";
                text_Task_Char.text = $"{sign = signs[Random.Range(0, signs.Length)]}";
                text_Task_B.text = $"{numb_B = Random.Range(0, 10)}";
                break;
            case 2:
                text_Task_A.text = $"{numb_A = Random.Range(10, 100)}";
                text_Task_Char.text = $"{sign = signs[Random.Range(0, signs.Length)]}";
                text_Task_B.text = $"{numb_B = Random.Range(10, 100)}";
                break;
            case 3:
                text_Task_A.text = $"{numb_A = Random.Range(100, 1000)}";
                text_Task_Char.text = $"{sign = signs[Random.Range(0, signs.Length - 2)]}";
                text_Task_B.text = $"{numb_B = Random.Range(100, 1000)}";
                break;
            default:
                Debug.LogWarning("Amount signs error");
                break;
        }
    }
    private void Set_Winner_Button_Text()
    {
        switch (sign)
        {
            case '+':
                result_Task = numb_A + numb_B;
                buttons_Text[numb_Winner_Button = Random.Range(0, buttons.Length)].text = result_Task.ToString();
                Addition_Set_Button_Text();
                break;

            case '-':
                result_Task = numb_A - numb_B;
                buttons_Text[numb_Winner_Button = Random.Range(0, buttons.Length)].text = result_Task.ToString();
                Subtraction_Set_Button_Text();
                break;

            case '*':
                result_Task = numb_A * numb_B;
                buttons_Text[numb_Winner_Button = Random.Range(0, buttons.Length)].text = result_Task.ToString();
                Multiplication_Set_Button_Text();
                break;

            case '/':
                result_Task =(float) numb_A / numb_B;
                buttons_Text[numb_Winner_Button = Random.Range(0, buttons.Length)].text = result_Task.ToString();
                Division_Set_Button_Text();
                break;

            default:
                Debug.LogWarning("error button text");
                break;
        }
    }
    public void Set_Next_Question()
    {
        Set_Question();
        Set_Winner_Button_Text();
    }
    private void Addition_Set_Button_Text()
    {
        switch (count_Signs_Numbers)
        {
            case 1:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if(i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-5, 5)).ToString();
                    }
                }
                break;
            case 2:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-50, 50)).ToString();
                    }
                }
                break;
            case 3:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-500, 500)).ToString();
                    }
                }
                break;
            default:
                Debug.LogWarning("Amount signs error");
                break;
        }
    }
    private void Subtraction_Set_Button_Text()
    {
        switch (count_Signs_Numbers)
        {
            case 1:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-5, 5)).ToString();
                    }
                }
                break;
            case 2:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-50, 50)).ToString();
                    }
                }
                break;
            case 3:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-500, 500)).ToString();
                    }
                }
                break;
            default:
                Debug.LogWarning("Amount signs error");
                break;
        }
    }
    private void Multiplication_Set_Button_Text()
    {
        switch (count_Signs_Numbers)
        {
            case 1:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-5, 5)).ToString();
                    }
                }
                break;
            case 2:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-50, 50)).ToString();
                    }
                }
                break;
            case 3:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-500, 500)).ToString();
                    }
                }
                break;
            default:
                Debug.LogWarning("Amount signs error");
                break;
        }
    }
    private void Division_Set_Button_Text()
    {
        switch (count_Signs_Numbers)
        {
            case 1:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-5, 5)).ToString();
                    }
                }
                break;
            case 2:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-50, 50)).ToString();
                    }
                }
                break;
            case 3:
                for (int i = 0; i < buttons_Text.Length; i++)
                {
                    if (i != numb_Winner_Button)
                    {
                        buttons_Text[i].text = (result_Task + Random.Range(-500, 500)).ToString();
                    }
                }
                break;
            default:
                Debug.LogWarning("Amount signs error");
                break;
        }
    }
}


