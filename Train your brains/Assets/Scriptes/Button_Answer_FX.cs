using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button_Answer_FX : MonoBehaviour
{
    private Text _text;
    private Game_Controller Game_Controller;
    private void Start()
    {
        _text = GetComponentInChildren<Text>();
        Game_Controller = FindObjectOfType<Game_Controller>();
    }
    public void Check_Result()
    {
        if(Game_Controller.Result_Task == float.Parse(_text.text))
        {
            Game_Controller.Set_Next_Question();
        }
    }
}
